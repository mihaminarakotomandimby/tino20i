FROM debian:10
RUN apt-get update
RUN apt-get install -y php7.3-bcmath php7.3-bz2 php7.3-cli php7.3-curl php7.3-gd php7.3-intl php7.3-json php7.3-mbstring  php7.3-mysql php7.3-pgsql php7.3-xml libapache2-mod-php7.3
RUN apt-get install -y apache2
EXPOSE 80
ADD ./webroot /var/www/html/
#RUN git clone git-fapbm /var/www/html/
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

